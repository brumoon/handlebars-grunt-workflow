module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        concat: {
            js: {
                src: ['js/1.js', 'js/2.js'], //TO-DO make this a pattern
                dest: 'build/js/site.js',
            },
            vendor: {
                src: ['js/vendor/1.js', 'js/vendor/2.js'], //TO-DO make this a pattern
                dest: 'build/js/vendor.js',
            },

            css: {
                src: ['css/1.css', 'css/2.css'], //TO-DO make this a pattern
                dest: 'build/css/styles.css',
            },

        },

        'compile-handlebars': {

            anyArray: {
                files: [{
                    src: ['templates/page-one.hbs', 'templates/page-two.hbs'],
                    dest: ['build/index.html', 'build/about-us.html']
    }],
                templateData: ['templates/data/page-one.json', 'templates/data/page-one.json'],
                partials: ['templates/partials/*.hbs', 'templates/components/*.hbs']
            },

        },

        watch: {
            options: {
                livereload: true,
            },
            css: {
                files: ['scss/*.scss'],
                tasks: ['sass', 'concat:css'],
            },

            js: {
                files: ['js/**/*.js'],
                tasks: ['concat:js'],
            },

            handlebars: {
                files: ['templates/**/*'],
                tasks: ['compile-handlebars'],
            }
        },

        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'build/css/creative.css': 'scss/creative.scss'
                }
            }
        },

        connect: {
            server: {
                options: {
                    port: 8000,
                    hostname: '*',
                    open: true,
                    base: 'build',
                    livereload: true
                }
            }
        }

    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-compile-handlebars');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');


    //            "grunt-compile-handlebars": "^2.0.2",
    //        "grunt-contrib-clean": "^1.1.0",
    //        "grunt-contrib-concat": "^1.0.1",
    //        "grunt-contrib-connect": "^1.0.2",
    //        "grunt-contrib-watch": "^1.0.1",
    //        "grunt-sass": "^2.1.0",

    grunt.registerTask('dev', ['sass', 'concat', 'compile-handlebars','connect', 'watch']);


};
